package week3.practice1.domain;

public enum Color {
    Green, Red, Purple, Cyan, Blue
}
