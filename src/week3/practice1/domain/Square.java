package week3.practice1.domain;

public class Square extends Rectangle {
    public Square(){
        // default Rectangle
    }

    public Square(double side){
        super(side, side);
    }

    public Square(Color color, boolean filled, double side){
        super(color, filled, side, side);
    }

    @Override
    public void setWidth(double side){
        setSide(side);
    }

    @Override
    public void setLength(double side){
        setSide(side);
    }

    public void setSide(double side){
        super.setLength(side);
        super.setWidth(side);
    }

    public double getSide(){
        return getLength();
    }

    public String toString(){
        return "A Square with side=" + getSide() + ", which is a subclass of " + super.toString();
    }
}
