package week3.practice1;

import week3.practice1.domain.Circle;
import week3.practice1.domain.Color;
import week3.practice1.domain.Shape;
import week3.practice1.domain.Square;

public class Main {

    public static void main(String[] args) {
        Shape shape1 = new Shape();
        Shape shape2 = new Shape(Color.Purple, true);
        System.out.println(shape1);
        System.out.println(shape2);
        shape1.setColor(Color.Cyan);
        shape1.setFilled(false);
        System.out.println(shape1.getColor() + " " + shape1.isFilled());

        Circle circle1 = new Circle();
        System.out.println(circle1);
        circle1.setRadius(2.0);
        System.out.println(circle1.getRadius());
        System.out.println(circle1.getArea() + " | " + circle1.getPerimeter());
        Circle circle2 = new Circle(Color.Red, true, 3.0);
        System.out.println(circle2);

        Square square1 = new Square(2.0);
        square1.setLength(5.0);
        System.out.println(square1);
        Square square2 = new Square(Color.Purple, false, 7.3);
        System.out.println(square2);

    }
}
